/root/.ssh:
  file.directory:
    - user: root
    - group: root
    - dir_mode: 0700
/root/.ssh/id_ed25519:
  file.managed:
    - user: root
    - group: root
    - mode: 600
    - makedirs: False
    - contents_pillar: keystuff:private
/root/.ssh/authorized_keys:
  file.managed:
    - user: root
    - group: root
    - mode: 600
    - makedirs: False
    - contents_pillar: keystuff:authorized
/root/.ssh/id_ed25519.pub:
  file.managed:
    - user: root
    - group: root
    - mode: 600
    - makedirs: False
    - contents_pillar: keystuff:authorized
