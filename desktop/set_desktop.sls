/etc/salt/minion.d/desktop.conf:
  file.managed:
    - contents: |
        grains:
          myrole: desktop

salt-minion:
  service.enabled:
    - full_restart: True
    - watch:
        - file: /etc/salt/minion.d/desktop.conf
