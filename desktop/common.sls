install_desktop_pkg:
  pkg.installed:
    - pkgs:
      - gnucash
      - mono-complete
