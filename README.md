
This is work in progress of keeping my home lab infrastructure in salt as
infrastructure as code.

At this point, I have salt, zabbix, and graylog mostly working.

Salt installs without any extra steps. Minions require manual bootstrapping
for me right now as I haven't gotten templates or cloud-init quite working.

Zabbix server required manual steps. These can likely be done with a custom script.
There is also some manual steps to have zabbix accept new agents automatically.

Graylog sidecar agent now installs without any extra steps. It is not included here
as it requires a token from the web UI and I haven't gotten that pillar data to
work correctly. Graylog server install requires several manual steps, espeically
since my current hardware doesn't include Sandybridge/AVX instructions, so
I ended up rebuilding MongoDB without those optimizations to let it run.
Opensearch requires some env var to install, which I couldn't see a way to do with
salt. 

Rancher2 has a number of bootstrap scripts, but currently they aren't working
completely.
