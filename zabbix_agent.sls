zabbix_agent2_conf:
  file.managed:
    - name: /etc/zabbix/zabbix_agent2.d/zabbix_arda_agent.conf
    - source: salt://zabbix_arda_agent.conf
    - makedirs: True
zabbix-agent2:
  pkg.installed: []
  service.running:
      - enable: True
      - watch:
        - file: /etc/zabbix/zabbix_agent2.d/zabbix_arda_agent.conf
