install_base_pkg:
  pkg.installed:
    - pkgs:
      - curl
      - sudo
      - vim
      - sysstat
      - screen
      - lm-sensors
      - sssd-ldap
      - autofs-ldap
      - nfs-common
      - git
      - gnupg
common simple sssd:
  file.managed:
    - name: /etc/sssd/sssd.conf
    - source: salt://sssd.conf
    - user: root
    - group: root
    - mode: 600
common simple homedirs:
  file.managed:
    - name: /etc/apparmor.d/tunables/home.d/arda.local
    - source: salt://arda.local
sssd:
  pkg.installed: []
  service.running:
    - enable: True
    - full_restart: True
    - watch:
      - file: /etc/sssd/sssd.conf
    - require:
      - pkg: sssd
apparmor:
  service.running:
    - enable: True
    - full_restart: True
    - watch:
      - file: /etc/apparmor.d/tunables/home.d/arda.local
autofs:
  service.running:
    - enable: True
    - full_restart: True
    - watch:
      - service: sssd
