install_packages:
  pkg.installed:
    - pkgs:
      - zfs-dkms
      - zfsutils-linux
      - sanoid
      - parted
/etc/sanoid/sanoid.default.conf:
  file.copy:
    - source: /usr/share/sanoid/sanoid.defaults.conf
    - makedirs: True
    - requires:
        pkg: sanoid
/etc/systemd/system/syncoid-pull.service:
  file.managed:
    - source: salt://zpool/syncoid-pull.service
/etc/systemd/system/syncoid-pull.timer:
  file.managed:
    - source: salt://zpool/syncoid-pull.timer
syncoid-pull.timer:
  service.running:
    - enable: true
    - requires:
      - file: /etc/systemd/system/syncoid-pull.timer
      - file: /etc/systemd/system/syncoid-pull.service



