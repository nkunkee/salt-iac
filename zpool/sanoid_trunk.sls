install_packages:
  pkg.installed:
    - pkgs:
      - zfs-dkms
      - zfsutils-linux
      - sanoid
      - parted
/etc/sanoid/sanoid.default.conf:
  file.copy:
    - source: /usr/share/sanoid/sanoid.defaults.conf
    - makedirs: True
    - requires:
        pkg: sanoid

