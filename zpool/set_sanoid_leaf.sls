/etc/salt/minion.d/sanoid_leaf.conf:
  file.managed:
    - contents: |
        grains:
          zfsrole: sanoid_leaf

salt-minion:
  service.enabled:
    - full_restart: True
    - watch:
        - file: /etc/salt/minion.d/sanoid_leaf.conf
