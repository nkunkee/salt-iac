/etc/salt/minion.d/sanoid_trunk.conf:
  file.managed:
    - contents: |
        grains:
          zfsrole: sanoid_trunk

salt-minion:
  service.enabled:
    - full_restart: True
    - watch:
        - file: /etc/salt/minion.d/sanoid_trunk.conf
