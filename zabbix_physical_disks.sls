smartmontools:
  pkg.installed: []
/etc/sudoers.d/arda-zabbix:
  file.managed:
    - contents: "zabbix ALL=(ALL) NOPASSWD:/usr/sbin/smartctl "
    - mode: 600
