# https://ranchergovernment.com/blog/article-simple-rke2-longhorn-and-rancher-install
open-iscsi:
  pkg.installed: []
nfs-common:
  pkg.installed: []
netscript-ipfilter:
  pkg.installed: []
apparmor-utils:
  pkg.installed: []
install_server:
  cmd.script:
    - name: install.sh
    - creates: /usr/local/lib/systemd/system/rke2-server.service
    - env:
      - INSTALL_RKE2_TYPE: server
    - source: https://get.rke2.io
rke2-server:
  service.running:
    - enable: True
    - watch:
      - cmd: install_server

