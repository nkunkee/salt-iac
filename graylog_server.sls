sysctl for greylog:
  file.managed:
    - name: /etc/sysctl.d/arda_greylog.conf
    - source: salt://arda_greylog.conf
# mongodb rebuilt from source to avoid AVX instructions and intalled by hand
# opensearch requires an env:OPENSEARCH_INITAL_ADMIN_PASSWORD to be exported before installing
#opensearch:
#  pkg.installed:
#    - sources:
#      - opensearch: https://artifacts.opensearch.org/releases/bundle/opensearch/2.14.0/opensearch-2.14.0-linux-x64.deb
#deb [signed-by=/etc/apt/keyrings/greylog-keyring.gpg] https://packages.graylog2.org/debian stable 6.0:
#  pkgrepo.managed:
#    - file: /etc/apt/sources.list.d/greylog-mylist.list
#    - key_url: https://packages.graylog2.org/repo/debian/keyring.gpg
#    - aptkey: False
graylog:
  pkg.installed:
    - sources:
      - graylog-6.0-repository: https://packages.graylog2.org/repo/packages/graylog-6.0-repository_latest.deb
# for reasons I don't understand, I had to manually unpack and install this
graylog-server:
  pkg.installed:
    - sources: []
    - refresh: True
  service.running:
    - enable: True
# manual steps to set password(s) for graylog and elasticsearch address
