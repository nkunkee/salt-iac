base:
  '*':
    - common_base
    - zabbix_agent
    - graylog_sidecar
  'erenmithrin*':
    - graylog_assemble
    - graylog_server
  'G@virtual:physical and G@disks:*':
    - zabbix_physical_disks
  'G@cpuarch:aarch64':
    - aarch64_journald
  'novaprime*':
    - rancher2_server
  'novasuper*':
    - rancher2_agent
  'novaalpha*':
    - rancher2_agent
  'halite*':
    - salt/salt_master
  'roke.kunkee':
    - zpool/set_sanoid_trunk
  'gont.kunkee':
    - zpool/set_sanoid_leaf
  'terminus*':
    - zpool/set_sanoid_leaf
  'G@zfsrole:sanoid_leaf':
    - zpool/sanoid_leaf
    - arda_common_keys
  'G@zfsrole:sanoid_trunk':
    - zpool/sanoid_trunk
    - arda_common_keys
  'G@myrole:desktop':
    - desktop/common
  'atlantis*':
    - desktop/set_desktop
  'roke*':
    - desktop/set_desktop
