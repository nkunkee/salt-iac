greylog_assemble:
  pkg.installed:
    - pkgs:
      - gcc-12
      - g++-12
      - build-essential
      - libcurl4-gnutls-dev
      - liblzma-dev
      - libssl-dev
      - python3-venv
      - curl
      - wget
      - libc++-dev
      - python-dev-is-python3
